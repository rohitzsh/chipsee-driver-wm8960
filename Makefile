DEPMOD  = /sbin/depmod
ARCH := arm
CROSS_COMPILE ?=
KVER  := $(shell uname -r)
KSRC := /lib/modules/$(KVER)/build
MODDESTDIR := $(INSTALL_MOD_PATH)/usr/lib/modules/$(KVER)/kernel/sound/soc/codecs/
MODULE_NAME := snd-soc-wm8960

# If KERNELRELEASE is defined, we've been invoked from the
# kernel build system and can use its language
#ifneq ($(KERNELRELEASE),)
#$(warning KERNELVERSION=$(KERNELVERSION))

#$(MODULE_NAME)-objs := wm8960.o 

#obj-m += $(MODULE_NAME).o

ifdef DEBUG
ifneq ($(DEBUG),0)
        ccflags-y += -DDEBUG -DAC101_DEBG
endif
endif

#else

$(MODULE_NAME)-y += wm8960.o

SRC := $(shell pwd)

all:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC)

modules_install:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC) modules_install

clean:
	rm -rf *.o *~ core .depend .*.cmd *.ko *.mod.c .tmp_versions modules.order Module.symvers

#endif
